#!/bin/bash
#使用方法nohup ./mindspore-collect.sh 20201010 20201020 &
#备份10月10日到10月20日的数据 nohup -终端断开任务也能在后台执行   20201010 -脚本执行的开始日期（包含此日期） 20201020 -脚本执行结束日期  & -后台执行。
startdate="$1"
date1=$(date -d "$1" "+%s")
date2=$(date -d "$2" "+%s")
date_count=$(echo "$date2 - $date1"|bc)
day_m=$(echo "$date_count"/86400|bc)
for ((sdate=0;sdate<"$day_m";sdate++))
do
        month=$(date -d "$startdate $sdate days" "+%m")
        day=$(date -d "$startdate $sdate days" "+%d")
    echo "logstash-2020$month.$t start"
    NODE_TLS_REJECT_UNAUTHORIZED=0 /usr/local/bin/elasticdump  --input=https://user:passwd@ip:port --input-index=logstash-2020.$month.$day --output=/mnt/mount_path/logstash-mindspore-2020.$month.$day-hk-app.json  --searchBody '{"query":{"term":{"kubernetes.labels.app.keyword": "indexname"}}}' --type=data &
    echo "logstash-2020$month.$t finished">> /home/xia/es_to_obs/trans22.log
        echo $month $day have done

done
