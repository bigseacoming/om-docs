# Install Guide

**Install with docker**

## Install Elasticsearch
[Reference https://www.elastic.co/guide/en/elasticsearch/reference/7.9/docker.html](https://www.elastic.co/guide/en/elasticsearch/reference/7.9/docker.html)

### Pull the image
```
docker pull docker.elastic.co/elasticsearch/elasticsearch:7.9.0
```

### Run Elasticsearch
```
docker run -d -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" --name elasticsearch docker.elastic.co/elasticsearch/elasticsearch:7.9.0
```

## Install Grafana
[Reference https://grafana.com/docs/grafana/latest/installation/docker/](https://grafana.com/docs/grafana/latest/installation/docker/)

### Run Grafana
```
docker run -d -p 3000:3000 --name grafana grafana/grafana
```

### Install piechart plugins in the Docker container
Execute the command in the Docker container
```
grafana-cli plugins install grafana-piechart-panel
```
Restart Docker container

## Install Om-collections
### Pull the image
```
docker pull swr.cn-north-4.myhuaweicloud.com/om/om-collection:0.0.9
```
### Run Om-collections
```
docker run  -v /local_path/config.ini:/var/lib/om/config.ini -v /local_path/users:/var/lib/om/users  -d  swr.cn-north-4.myhuaweicloud.com/om/om-collection:0.0.9
```